import java.util.Random;
public class Die {
	private int faceValue;
	private Random randomNum;
	
	public Die() {
		this.faceValue=1;
		this.randomNum=new Random();
	}
	
	public int getFaceValue() {
		return this.faceValue;
	}
	
	public void roll() {
		this.faceValue=randomNum.nextInt(5)+1;
	}
	
	public String toString() {
		return ""+getFaceValue();
	}
}
		